export interface Hero {
  name: string;
  bio: string;
  img: string;
  appearance: string;
  home: string;
}
