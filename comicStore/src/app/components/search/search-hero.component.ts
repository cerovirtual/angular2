import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Hero} from '../../interfaces/hero.interface';
import {HeroesService} from '../../services/heroes.service';

@Component({
  selector: 'app-search-hero',
  templateUrl: './search-hero.component.html'
})
export class SearchHeroComponent implements OnInit {

  heroes: Hero[] = [];
  term: string;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _heroesService: HeroesService,
    private _router: Router
  ) {

  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(
      params => {
        this.term = params['term'];
        this.heroes = this._heroesService.findHeroes(this.term);
      }
    );
  }

  viewHero(id: number) {
    this._router.navigate(['/hero', id]);
  }
}
