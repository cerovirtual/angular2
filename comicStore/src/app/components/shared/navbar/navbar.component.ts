import {Component, OnInit} from '@angular/core';
import {Hero} from '../../../interfaces/hero.interface';
import {Router} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html'
})
export class NavbarComponent implements OnInit {

  heroes: Hero[] = [];

  constructor(
    private _router: Router
  ) { }

  ngOnInit() { }

  searchHero(inputTerm: string) {
    this._router.navigate(['/search', inputTerm]);
  }
}
