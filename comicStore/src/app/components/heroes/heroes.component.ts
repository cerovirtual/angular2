import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {HeroesService} from '../../services/heroes.service';
import {Hero} from '../../interfaces/hero.interface';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html'
})
export class HeroesComponent implements OnInit {

  heroes: Hero[] = [];

  constructor(
    private _heroesService: HeroesService,
    private _router: Router
  ) {

  }

  ngOnInit() {
    this.heroes = this._heroesService.getHeroes();
  }

  viewHero(id: number) {
    this._router.navigate(['/hero', id]);
  }
}
