import { FirstTestComponent } from './first-test';

describe('First Tests', () => {
  it('true is true', () => expect(true).toBe(true));

  it('multiplying should work', () => {
    expect(4 * 4).toEqual(16);
  });
});

describe('Testing message state in message.component', () => {
  let app: FirstTestComponent;

  beforeEach(() => {
    app = new FirstTestComponent();
  });

  it('should set new message', () => {
    app.setMessage('Hello world');
    expect(app.message).toBe('Hello world');
  });

  it('should clear message', () => {
    app.clearMessage();
    expect(app.message).toBe('');
  });
});
