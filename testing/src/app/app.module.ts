import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FirstTestComponent } from './first-test';
import { QuoteComponent } from './quote.component';

@NgModule({
  declarations: [
    AppComponent,
    FirstTestComponent,
    QuoteComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
