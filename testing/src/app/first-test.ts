import { Component } from '@angular/core';

@Component({
  selector: 'app-first-test',
  template: ``
})
export class FirstTestComponent {
  message: string = '';

  constructor() {}

  setMessage(newMessage: string) {
    this.message = newMessage;
  }

  clearMessage() {
    this.message = '';
  }
}
