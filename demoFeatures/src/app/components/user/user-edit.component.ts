import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-edit',
  template: `
    <p>
      user-edit works!
    </p>
  `,
  styles: []
})
export class UserEditComponent implements OnInit {

  constructor(private _activatedRoute: ActivatedRoute) {
    this._activatedRoute
      .parent
      .params
      .subscribe(params => {
        console.log('Child Edit params: ', params);
      });
  }

  ngOnInit() {
  }

}
