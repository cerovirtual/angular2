import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-view',
  template: `
    <p>
      user-view works!
    </p>
  `,
  styles: []
})
export class UserViewComponent implements OnInit {

  constructor(private _activatedRoute: ActivatedRoute) {
    this._activatedRoute
      .parent
      .params
      .subscribe(params => {
        console.log('Child View params: ', params);
      });
  }

  ngOnInit() {
  }

}
