import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-css',
  template: `
    <h2>CSS Scope</h2>
    <p>
      Hello from the css.component
    </p>
  `,
  styles: [`
    h2 {
      margin-top: 40px;
    }
    
    p {
      color: red;
    }
  `]
})
export class CssComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
