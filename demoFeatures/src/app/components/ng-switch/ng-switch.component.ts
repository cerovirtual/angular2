import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-switch',
  templateUrl: './ng-switch.component.html'
})
export class NgSwitchComponent implements OnInit {

  alertTypes = [
    'alert-success',
    'alert-danger',
    'alert-warning',
    'alert-info'
  ];

  constructor() { }

  ngOnInit() {
  }

}
