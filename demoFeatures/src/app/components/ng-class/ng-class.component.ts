import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-class',
  templateUrl: './ng-class.component.html',
  styles: [`
    #alert-types, #simulate-save{
      margin: 20px;
    }
  `]
})
export class NgClassComponent implements OnInit {

  alertShow = 0;
  alertTypes = [
    'alert-success',
    'alert-danger',
    'alert-warning',
    'alert-info'
  ];

  loading: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  simulateSave() {
    this.loading = true;
    setTimeout( () => {
      this.loading = false;
    }, 3000);
  }

  changeAlert() {
    if (this.alertShow + 1 <= 3) {
      this.alertShow ++;
    } else {
      this.alertShow = 0;
    }
  }
}
