import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appHighlighted]'
})
export class HighlightedDirective {

  @Input('appHighlighted') newColor: string;

  @HostListener('mouseenter') onMouseEnter() {
    if (this.newColor) {
      this._elementRef.nativeElement.style.backgroundColor = this.newColor;
    } else {
      this._elementRef.nativeElement.style.backgroundColor = '#f8d7da';
    }
  }

  @HostListener('mouseleave') onMouseLeave() {
    this._elementRef.nativeElement.style.backgroundColor = '#fff3cd';
  }

  constructor(private _elementRef: ElementRef) {
    _elementRef.nativeElement.style.backgroundColor = '#d1ecf1';
  }

}
