import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { UserComponent } from './components/user/user.component';
/*import { UserNewComponent } from './components/user/user-new.component';
import { UserEditComponent } from './components/user/user-edit.component';
import { UserViewComponent } from './components/user/user-view.component';*/
import { APP_ROUTES_USER } from './components/user/user.routes';

const APP_ROUTES: Routes = [
  { path: 'home', component: HomeComponent },
  {
    path: 'user/:id',
    component: UserComponent,
    /*children: [
      { path: 'new', component: UserNewComponent },
      { path: 'edit', component: UserEditComponent },
      { path: 'view', component: UserViewComponent },
      { path: '**', pathMatch: 'full', redirectTo: 'view' }
    ]*/
    children: APP_ROUTES_USER
  },
  { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
