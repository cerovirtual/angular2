export class Hero {
  name: string;
  home: string;
  bio: string;
  key$?: string;

  constructor(obj?: any) {
    this.name = obj && obj.name || '';
    this.home = obj && obj.home || 'Marvel';
    this.bio = obj && obj.bio || '';
  }
}
