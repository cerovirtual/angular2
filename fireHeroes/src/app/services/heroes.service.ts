import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { Hero } from '../models/hero.model';

@Injectable()
export class HeroesService {
  private baseAPIUrl = 'https://fireheroes-deb5f.firebaseio.com';

  constructor(private http: HttpClient) { }

  private buildRequest(body: Object): any {
    return {
      body: JSON.stringify(body),
      headers: new Headers({
        'Content-type': 'application/json'
      })
    };
  }

  newHero(hero: Hero): Observable<object> {
    const request = this.buildRequest(hero);

    return this.http
      .post(
        `${this.baseAPIUrl}/heroes.json`,
        request.body,
        {
          responseType: 'json',
          headers: request.headers
        }
      );
  }

  updateHero(id: string, hero: Hero): Observable<object> {
    const request = this.buildRequest(hero);

    return this.http
      .put(
        `${this.baseAPIUrl}/heroes/${id}.json`,
        request.body,
        {
          responseType: 'json',
          headers: request.headers
        }
      );
  }

  deleteHero(id: string): Observable<object> {
    return this.http
      .delete(`${this.baseAPIUrl}/heroes/${id}.json`);
  }

  getHero(id: string): Observable<object> {
    return this.http
      .get<Hero>(`${this.baseAPIUrl}/heroes/${id}.json`);
  }

  getHeroes(): Observable<object> {
    return this.http
      .get(`${this.baseAPIUrl}/heroes.json`);
  }

}
