import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { HeroesService } from '../../services/heroes.service';
import { Hero } from '../../models/hero.model';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styles: []
})
export class HeroesComponent {

  heroes: Hero[] = [];

  constructor(
    private heroesService: HeroesService,
    private router: Router
  ) {
    this.heroesService
      .getHeroes()
      .subscribe(res => {
          if (res) {
            for (const [key, value] of Object.entries(res)) {
              const hero: Hero = value;

              hero.key$ = key;
              this.heroes.push(hero);
            }
          }
        },
        error => {
          console.log('error fetching heroes:', error);
        });
  }

  delete(id: string) {
    this.heroesService
      .deleteHero(id)
      .subscribe(res => {
          for (const [key, value] of Object.entries(this.heroes)) {
            if (id === value.key$) {
              this.heroes.splice(Number(key), 1);
            }
          }
        },
        error => {
          console.log('error fetching heroes:', error);
        });
  }

}
