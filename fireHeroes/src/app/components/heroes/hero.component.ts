import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { Hero } from '../../models/hero.model';
import { HeroesService } from '../../services/heroes.service';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styles: []
})
export class HeroComponent {

  hero: Hero;
  id: string;

  constructor(
    private heroesService: HeroesService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.initHero();
    this.activatedRoute
      .params
      .subscribe(params => {
        this.id = params['id'];
        if ('new' !== this.id) {
          this.heroesService
            .getHero(this.id)
            .subscribe(data => this.hero = <Hero>data);
        }
      });
  }

  initHero(): void {
    this.hero = new Hero();
  }

  new(heroForm: NgForm) {
    this.router.navigate(['hero', 'new']);
    this.initHero();
    heroForm.reset(this.hero);
  }

  save() {
    if ('new' === this.id) {
      this.heroesService
        .newHero(this.hero)
        .subscribe(data => {
            this.router.navigate(['/hero', data['name']]);
          },
          error => {
            console.log('Save error: ', error);
          });
    } else {
      this.heroesService
        .updateHero(this.id, this.hero)
        .subscribe(data => {
            console.log('hero updated', data);
          },
          error => {
            console.log('Update error: ', error);
          });
    }
  }

}
