import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MoviesService {

  private apiKey = 'e020935a036e7a5ff181b825ded3975f';
  private apiUrl = 'https://api.themoviedb.org/3';
  movie: any;
  results: any;

  constructor(private http: HttpClient) {
  }

  getCurrentInTheaters(): Observable<any> {
    const start = new Date();
    const end = new Date();

    end.setDate(start.getDate() + 14);
    const startStr = `${start.getFullYear()}-${start.getMonth()}-${start.getDate()}`;
    const endStr = `${end.getFullYear()}-${end.getMonth()}-${end.getDate()}`;

    return this.http
      .get(
        `${this.apiUrl}/discover/movie?primary_release_date.gte=${startStr}&primary_release_date.lte=${endStr}&api_key=${this.apiKey}&language=es`
      );
  }

  getMovie(movieId: string): Observable<any> {
    return this.http
      .get(
        `${this.apiUrl}/movie/${movieId}?api_key=${this.apiKey}&language=es`
      );
  }

  getPopular(): Observable<any> {
    return this.http
      .get(
        `${this.apiUrl}/discover/movie?sort_by=popularity.desc&api_key=${this.apiKey}&language=es`
      );
  }

  getPopularKids(): Observable<any> {
    return this.http
      .get(
        `${this.apiUrl}/discover/movie?certification_country=US&certification.lte=G&sort_by=popularity.desc&api_key=${this.apiKey}&language=es`
      );
  }

  searchMovies(term: string): Observable<any> {
    return this.http
      .get(
        `${this.apiUrl}/search/movie?query=${term}&api_key=${this.apiKey}&language=es`
      );
  }

}
