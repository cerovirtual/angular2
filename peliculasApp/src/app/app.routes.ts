import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { SearchComponent } from './components/search/search.component';
import { MovieComponent } from './components/movie/movie.component';

const heroes_routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'search', component: SearchComponent },
  { path: 'search/:term', component: SearchComponent },
  { path: 'movie/:id', component: MovieComponent },
  { path: '**', pathMatch: 'full', redirectTo: '/home' },
];

export const routes = RouterModule.forRoot(heroes_routes);
