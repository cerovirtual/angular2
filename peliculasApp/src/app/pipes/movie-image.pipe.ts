import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'movieImage'
})
export class MovieImagePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const url = 'http://image.tmdb.org/t/p/w500';

    if (value.poster_path) {
      return `${url}${value.poster_path}`;
    } else if (value.backdrop_path) {
      return `${url}${value.backdrop_path}`;
    } else {
      return 'assets/img/no-image.png';
    }
  }

}
