import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { MoviesService } from '../../services/movies.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: []
})
export class SearchComponent implements OnInit {

  movies: any;
  search: string;

  constructor(private route: ActivatedRoute,
              private movieService: MoviesService) {
    this.movies = this.movieService.results;

    this.route
      .params
      .subscribe(params => {
        if (params['term']) {
          this.search = params['term'];
          this.searchMovies();
        }
      });
  }

  searchMovies(): void {
    this.movieService
      .searchMovies(this.search)
      .subscribe(data => {
        this.movies = data.results;
        this.movieService.results = data.results;
      });
  }

  ngOnInit() {
  }

}
