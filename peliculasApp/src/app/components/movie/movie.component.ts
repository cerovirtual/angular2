import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { MoviesService } from '../../services/movies.service';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styles: []
})
export class MovieComponent implements OnInit {

  movie: any;

  constructor(private route: ActivatedRoute,
              private location: Location,
              public movieService: MoviesService) {
    this.route
      .params
      .subscribe(params => {
        if (params['id']) {
          this.searchMovie(params['id']);
        }
      });
  }

  ngOnInit() {
  }

  backTo(): void {
    this.location.back();
  }

  searchMovie(movieId: string): void {
    this.movieService
      .getMovie(movieId)
      .subscribe(res => {
        this.movie = res;
        this.movieService.movie = res;
      });
  }

}
