import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-collage',
  templateUrl: './collage.component.html',
  styles: []
})
export class CollageComponent implements OnInit {

  @Input() movies: any;
  @Input() title: string;

  constructor() { }

  ngOnInit() {
  }

}
