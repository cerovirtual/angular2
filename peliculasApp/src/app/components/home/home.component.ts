import { Component, OnInit } from '@angular/core';

import { MoviesService } from '../../services/movies.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {

  inTheaters: any;
  popular: any;
  popularKids: any;

  constructor(private movieService: MoviesService) {
    this.movieService
      .getCurrentInTheaters()
      .subscribe(data => this.inTheaters = data.results);

    this.movieService
      .getPopular()
      .subscribe(data => this.popular = data.results);

    this.movieService
      .getPopularKids()
      .subscribe(data => this.popularKids = data.results);
  }

  ngOnInit() {
  }

}
