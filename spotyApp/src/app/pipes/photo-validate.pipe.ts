import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'photoValidate'
})
export class PhotoValidatePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value.length > 0 && value[1].url) {
      return value[1].url;
    } else {
      return 'assets/img/noimage.png';
    }
  }
}
