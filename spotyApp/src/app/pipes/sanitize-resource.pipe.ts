import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'sanitizeResource'
})
export class SanitizeResourcePipe implements PipeTransform {

  constructor(private _domSanitizer: DomSanitizer) {}

  transform(value: string, baseUrl: string): any {
    const resourceUrl = baseUrl + value;

    return this._domSanitizer
      .bypassSecurityTrustResourceUrl(resourceUrl);
  }
}
