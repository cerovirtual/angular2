import { Component, OnInit } from '@angular/core';

import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html'
})
export class SearchComponent implements OnInit {

  public term = '';

  constructor(private _spotify: SpotifyService) { }

  ngOnInit() {
  }

  searchArtist() {
    if (this.term.length > 0) {
      this._spotify
        .getArtists(this.term)
        .subscribe( data => {
          // store the artists in the service,
          // so we can get back to the search page
          // and keep them
          this._spotify.artists = data;
        });
    }
  }
}
