import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html'
})
export class ArtistComponent implements OnInit {

  public artist: any;
  public topTracks: any;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _spotify: SpotifyService
  ) { }

  ngOnInit() {
    this._activatedRoute
      .params
      .subscribe(params => {
        this._spotify
          .getArtist(params['id'])
          .subscribe( data => {
            this.artist = data;
          });

        this._spotify
          .getArtistTopTracks(params['id'])
          .subscribe( data => {
            this.topTracks = data;
          });
      });
  }
}
