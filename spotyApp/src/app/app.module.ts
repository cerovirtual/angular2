import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

/* Components */
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { SearchComponent } from './components/search/search.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { ArtistComponent } from './components/artist/artist.component';

/* Routes */
import { APP_ROUTING } from './app.routes';

/* Services */
import { SpotifyService } from './services/spotify.service';

/* Pipes */
import { PhotoValidatePipe } from './pipes/photo-validate.pipe';
import { SanitizeResourcePipe } from './pipes/sanitize-resource.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SearchComponent,
    NavbarComponent,
    PhotoValidatePipe,
    ArtistComponent,
    SanitizeResourcePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    APP_ROUTING
  ],
  providers: [SpotifyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
