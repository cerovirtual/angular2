import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class SpotifyService {

  public artists: any;
  public baseUrl = 'https://api.spotify.com/v1/';
  public country = 'US';
  private _token = 'BQB5A8d7c9DnBdMWqmox0XzdiJ2AluP7yUVpQIV78xMcuy4v3pNc_sEsiCF_HLENsQvrsPBqGlbxAiPQjrARxQ';

  constructor(private _http: Http) { }

  getArtists(term: string) {
    const searchUrl = `${ this.baseUrl }search?q=${ term }&type=artist`;
    const headers = new Headers();
    headers.append('Authorization', `Bearer ${ this._token }`);

    return this._http
      .get(searchUrl, { headers })
      .map( res => {
        console.log(res.statusText);
        return res.json().artists.items;
      });
  }

  getArtist(id: string) {
    const searchUrl = `${ this.baseUrl }artists/${ id }`;
    const headers = new Headers();
    headers.append('Authorization', `Bearer ${ this._token }`);

    return this._http
      .get(searchUrl, { headers })
      .map( res => {
        console.log(res.statusText);
        return res.json();
      });
  }

  getArtistTopTracks(id: string) {
    const searchUrl = `${ this.baseUrl }artists/${ id }/top-tracks?country=${ this.country }`;
    const headers = new Headers();
    headers.append('Authorization', `Bearer ${ this._token }`);

    return this._http
      .get(searchUrl, { headers })
      .map( res => {
        console.log(res.statusText);
        return res.json().tracks;
      });
  }
}
